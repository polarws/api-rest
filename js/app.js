// primero establece cuales es la URL de la que obtener los datos //
var BASE_URL='http://eimtcms.uoc.edu/~cbusquets89/wordpressapi';
var ROOT_URL = BASE_URL+'/wp-json/wp/v2';

// aplicación
var APP = {

	// al iniciar la app, inicia las siguientes funciones: crea el filtro, el botón para poder volver atrás
	// y obtiene los posts
    init: function () {
    	APP.createFilter();
    	APP.createBackButton();
        APP.getPosts();
    },

	// creación del botón para poder volver atrás
	createBackButton : function(){
        // coger el elemento del botón mediante la clase de CSS
        var myDivElement = $( ".back-to-movie-list" );

        // añadirle una función al elemento del btn
        myDivElement.click(function(e) {
        	// se evita el comportamiento "normal"
            e.preventDefault();
            // la función de clic muestra la lista de pelis y oculta la ficha de la película que
			// estaba abierta en ese momento
            APP.enableMovieList(true);
            APP.enableFilters(true);
            APP.enableMoviePanel(false, null);

            return false;
        });
	},

	// obtiene los posts de "movies" del json.
    getPosts: function () {
        $.ajax({
            type: 'GET',
            url: ROOT_URL + '/movies',
            dataType: 'json',
			// si encuentra posts inicia la función renderMovie.
			// el "each" irá obteniendo todos los elementos hasta que acabe.
            success: function (movies) {
                $.each(movies, function (index, movie) {
                    APP.renderMovie(movie)
                });
                $("ul.post-list").listview("refresh");
            },
			// muestra error si "movies" no tiene contenido, porque no hay nada que mostrar
            error: function (error) {
                console.log(error);
            }
        });
    },

	// crea listado de cada película, buscando el nombre de cada género (taxonomía) mediante
	// la ID que aparece en la URL de cada uno de ellos.
    renderMovie: function (movie) {
    	var url = movie._links["wp:term"][0].href;
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'json',
			// si hay elementos, los generará creando un <li> dentro del ul.post-list, que es el
			// elemento que seleccionamos
            success: function (genre) {
                 var element =  $('ul.post-list');
                 element.append(
                            '<li class="post-list__item">' +
                            '<a href=""><h3>' + movie.title.rendered + '</h3></a>' +
                            '<p class="post-list__item-genre">' + genre[0].name + '</p>' +
                            '</li>'
                        );
                 // Se selecciona el enlace para añadirle un comportamiento distinto al original
               var a = element.find('a').last();
                a.click(function(e) {
                    // se elimina la función "normal" de un link
                    e.preventDefault();
                    // ahora al hacer clic esconderá el listado con todas las películas
					// y mostrará la ficha de la escogida.
                    APP.enableMovieList(false);
                    APP.enableFilters(false);
                    APP.enableMoviePanel(true, movie, genre);

                	return false;
                });
                element.listview("refresh");
            },
            // muestra error si la película no tiene contenido, porque no hay nada a mostrar
            error: function (error) {
                console.log(error);
            }
        });
    },

	// define el comportamiento de MovieList, para poder definir cuando se ve
	// y cuando se oculta (él y sus <li> de dentro).
	enableMovieList: function(isVisible) {
        if (isVisible) {
        	 $('ul.post-list').css('display', 'block');
        }
        else
        {
            $('ul.post-list').css('display', 'none');
        }
	},


    // define el comportamiento de MoviePanel, para poder definir qué se ve
    // cuando se muestra - if (isVisible) y else.
	enableMoviePanel: function(isVisible, movie, genre) {
    	if (isVisible) {
    		// coge el div movie.panel-item y le añadimos contenido (con el element.append
			// de más adelante
			var element = $('.movie-panel-item');
            // limpia los elementos que ya había
			element.empty();

    		// pinta el HTML con los elementos de cada película: título, imagen de portada,
			// datos varios y el enlace para ver el Trailer en YouTube.
            element.append(
            	'<h2 class="post-list">' + movie.title.rendered + '</h2>' +
                '<img src="' + movie.cmb2.test_metabox._cbusquets_movie_image + '" />' +
                '<p>'+' Director: '+' '+ movie.cmb2.test_metabox._cbusquets_director + '</p>' +
                '<p>' + genre[0].name + '</p>' +
                '<p>'+' Movie lenght: '+' '+ movie.cmb2.test_metabox._cbusquets_length + '</p>' +
                '<p>'+' Year: '+' '+ movie.cmb2.test_metabox._cbusquets_year + '</p><br/>' +
				'<p>' + movie.cmb2.test_metabox._cbusquets_sinopsis + '</p><br/>' +
				'<a href=" ' + movie.cmb2.test_metabox._cbusquets_trailer + ' " target="_blank">'+'Click here to watch trailer'+'</a>' +
				'<br/><br/>'
        );

        $('.movie-panel').css('display', 'block');
    }

    else
		{
            $('.movie-panel').css('display', 'none');
		}
    },

    // hace una petición a todos los géneros con ajax, así podemos empezar a crear el filtro.
    createFilter : function(){
        $.ajax({
            type: 'GET',
			// toma la URL y le añade /genre, para poder cargar el listado con todos ellos
			// y sus ID, nombres, etc.
            url: ROOT_URL + '/genre',
            dataType: 'json',
            success: function (genre) {
                // si encuentra contenido obtendrá todos los géneros.
				// se realiza con $.each porque habrá más de uno.
                $.each(genre, function (index, genre) {
                    APP.renderGenre(genre)
                });
                $("ul.post-list").listview("refresh");
            },
            error: function (error) {
                console.log(error);
            }
        });


	},

	// con los géneros listados, hace lo siguiente:
	renderGenre: function(genre) {
        // coge el div donde estarán los filtros
        var element = $('.filter-genre');
        // le añade un enlace para poder acceder a ellos
        element.append(
            '<a href="#" wpid="'+genre.id+'">' + genre.name + '</a>'
        );

        // seleccionamos cada enlace
        var a = element.find('a').last();
        a.click(function(e) {
            // se evita el comportamiento "normal"
            e.preventDefault();
            // obtiene la ID de cada género
			var genreId = e.currentTarget.attributes[1].value;

			// hace la petición con el filtro, siguiendo la URL del json de WP
			var filterGenreUrl = '/movies?genre=' + genreId;
            $.ajax({
                type: 'GET',
				// define la URL de cada género
                url: ROOT_URL + filterGenreUrl,
                dataType: 'json',
                success: function (movies) {
                    // cuando termine, en el success, actualiza la lista de películas
					// coge el elemento de las pelis
					var element = $('ul.post-list');
					// limpia el elemento de las pelis con el empty
					element.empty(); // esto limpia el innerHtml de ul.post-list.

					// recorre las movies con el each y pinta cada peli por separado
					$.each(movies ,function(index, movie){
						// pinta la peli en el elemento de pelis, mirar el objeto movie que contiene

						APP.renderMovie(movie);
					});

                },
                error: function (error) {
                    console.log(error);
                }
            });
            return false;
        });

	},

	// define el comportamiento de enableFilters, para poder definir qué se ve
	// cuando se muestra - if (isVisible) y else.
	enableFilters : function(isVisible){
        if (isVisible) {
            $('.filter-genre').css('display', 'flex');
        }
        else
        {
            $('.filter-genre').css('display', 'none');
        }
	}

};